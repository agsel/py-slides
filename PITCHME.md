## ITI0101 - Programmeerimise algkursus
#### Loeng 1 - Korraldus

---

## Kokkuvõte

@ul 

- 8 loengut (paaritu nädal T10)
- 16 praktikumi: T12, N8, N10 
- 8 süvendatud praktikumi: paaris T10 
- Kursuse jooksul lahendate ülesandeid **iseseisvalt**
- Ülesande lahendamine ja **ettenäitamine** annab punkte 
- Punktid liidetakse 
- Semestri jooksul 400 punkti 
- Eksam 600 punkti 
- Kokku 501p - 600p => "1", ... 901p - .. => "5" 

@ulend

---

## Konsultatsioon

- Toimub ??
- Eelnevalt vaja registreerida (ained.ttu.ee kursuse lehel)
- Konsultatsioonis räägime u 30 min eelneva nädala teemad üle
- Saab küsida koduste ülesannete kohta

---

## Ainele registreerimine

- ained.ttu.ee -> uniid login
- suunatakse Microsofti lehele
 - email: uniid@ttu.ee (nt aglube@ttu.ee)
  - sellega saate ühtlasi oma e-kirju lugeda mail.ttu.ee lehel
 - parool (kui ei tea, siis pass.ttu.ee lehel saate luua/muuta)
- otsige aine "ITI0102 - Programmeerimise algkursus"
- registreeruge (*enrol*)

---

## Ülesanded

- Iga nädal kaks ülesannet
 - PR ülesanne tunnis tegemiseks
 - EX ülesanne kodus lahendamiseks
- Automaattestimine
- Kokku 15 nädalat

---

## Ülesannete tähtajad

- PR ülesanne praktikumi lõpus
 - Hiljem saab 0p
- EX ülesanne järgmise nädala alguseks
 - kuni 50% punktidest saab üks nädal hiljem
 - edasi 0p

- Näiteks EX01 tähtaeg on 10. september 23:59 (saab kuni 100%)
- kuni 17. september 23:59 saab kuni 50%
- alates 18. septembrist 0p

---

## Ülesande esitamine

- Ülesande lahendus laetakse Giti
- Tudeng saab esitatud ülesande kohta TTÜ emailile tagasisidet (näiteks 80% punktidest)
- Lahendust võib üles laadida (esitada) lõpmatu arv kordi (kui pole öeldud teisiti)
- Arvesse läheb parim (kui pole öeldud teisiti)
- Punktid lähevad arvesse alles pärast seda, kui lahendus on õppejõule ette näidatud
- Ettenäitamisel veendume, et tudeng saab **enda** kirjutatud koodist aru
- Samuti anname ettenäitamisel tudengi koodile tagasisidet

---

## Ülesande punktid

- Lõpptulemus sõltub kolmest komponendist:
 - automaattestid (100% =>15p; 80% => 13p)
 - stiilikontroll (korras => 1p, üks viga => 0)
 - ettenäitamine kuni 1p
- Saadud komponendid korrutatakse
 - näiteks: ``13 * 1 * 1 => 13p``.
- Kui stiil on korrast ära, siis on kokku 0p
- Kui ülesanne pole ette näiatud, siis on kokku 0p

---

## Automaatne stiilikontroll

 - Kõik lahendused peavad vastama stiilinõuetele
 - Kasutame aines kahte kontrolli:
  - PEP 8: https://www.python.org/dev/peps/pep-0008/ - Üldine koodistiil
  - PEP 257: https://www.python.org/dev/peps/pep-0257/ - Dokumenteerimise stiil
 - Kõik funktsioonid, klassid, meetodid jms peavad olema kommenteeritud
  - Sisu kontrollime ka ("asd" võib automaattesterile sobida, aga õppejõule mitte!)
 - Automaattester annab stiilivigadest teada

---

## Punktid kokku

- Ülesanded 400p
 - PR ülesanded 15 x 5p = 75p
 - EX ülesanded 15 x 15p = 225p
 - süvaülesanded 100p
- Eksam 600p
- Kokku: 1000p
- Kui süva ülesandeid ei tee, siis kokku 900p
- 901p -> "5", 801p -> "4", ... 501p -> "1", <501p -> "0" 

---

## Tunnikontrollid

* 5. nädalal tunnikontroll (15-20min)
  * antakse 5 ülesannet
  * vähemalt üks tuleb ära teha (20%)
  * vaadake codingbat.com pigem 1 taseme ülesanded
* 10. nädalal kontrolltöö (terve praktikum)
  * vähemalt pool tuleb ära teha (50%)

---

## Eksam

- **Eksamieeldus:**
 - vähemalt 200p
 - TK 20%, KT 50%
- Eksam toimub arvutiga
- Lahendatakse ülesandeid
- Interneti kasutamine pole lubatud
- Aega 4h

---

## Teemad nädalate kaupa

* 1n Sissejuhatus, muutuja, tingimuslause, sisendi lugemine
* 2n funktsioon, matemaatilised avaldised
* 3n sõne
* 4n järjend, tsükkel
* 5n sõnastik
* 6n sõne, regulaaravaldised
* 7n järjend, tsükkel jm kordamine
* 8n failid (lugemine, kirjutamine), sortimine

---

## Teemad (jätkub)


* 9n Rekursioon
* 10n Objektid, klassid
* 11n Objektid, klassid
* 12n Objektid, klassid, pärimine
* 13n Veebist lugemine
* 14n Vaba struktuuriga OOP
* 15n Vaba struktuuriga OOP
* 16n Kordamine