## Muutuja
#### *variable*

---

## Muutuja

- Kokkupuude matemaatikas ("Leia *x*")
- Programmeerimises on muutuja nimega viide "väärtusele"
- Pythonis muutujal ei ole andmetüüpi (mõnes keeles on)
- "Väärtusel" aga on andmetüüp

```python
age = 18
name = "Student"
```

---

## Muutuja kasutamine

```python
greeting = "Hello all!"
student_count = 300

print(greeting)
print("Students:", student_count)

greeting = "Hi"                     # value is changed
student_count = student_count + 2   # add 2

print(greeting)
print("Students:", student_count)

greeting += " students!"            # same as greeting = greeting + " .. "
print(greeting)
```

@[1](Loome muutuja ``greeting`` ja anname sellele tekstilise väärtuse (sõne))
@[2](Loome muutuja ``student_count`` ja anname sellele täisarvulise väärtuse)
@[4](Prindime tervituse)
@[5](Prindime tudengite arvu)
@[7](Muudame tervituse väärtust)
@[8](Suurendame tudengite arvu kahe võrra)
@[10](Prindime uue tervituse)
@[11](Prindime uue tudengite arvu)
@[13](Lisame tervitusele natuke teksti. `a += 1` saab kasutada `a = a + 1` asemel.)
@[14](Prindime uuendatud tervituse)


---

## Muutuja nimetamine

- Pythonis kasutame väikeseid tähti
- Mitmesõnalised muutujad eraldatakse allkriipsuga (_)
- Pigem kasutada mõtestatud nimetusi (``age``), mitte lühendeid/suvalisi tähistusi (``a``).

---

## Muutuja Pythonis

- Pythonis on iga "asi" objekt, st iga väärtus on objekt
- Muutuja on nimi, mis viitab mingile objektile
- Objektidest räägime aga hiljem (u 11. nädal)
- 